import json
from bs4 import BeautifulSoup
import requests
import csv
import sys

IRS_990_CSV_FILE = "irs_990.csv"

with open('index_2015.json') as irs990_data_file:
    data = json.load(irs990_data_file)

#Store URLs for each filing from the previously opened JSON file to mainURLList
mainURLList = []
for entry in data["Filings2015"] :
    mainURLList.append(entry['URL'])



master_data_dict_list = []

#For first 1000 URLs, parse for certain data from each xml file using Beautiful Soup
for urlNumber in range(0,1000):
    try:
        data_dict = {}
        r = requests.get(mainURLList[urlNumber])
        soup = BeautifulSoup(r.text,"lxml")

        #Get header and data parts from the xml file
        returnHeader = soup.find("returnheader")
        returnData =  soup.find("returndata")

        #Get EIN
        data_dict["EIN"] = returnHeader.find("filer").find("ein").find(text=True)

        #Get name of business. There are multiple formats. Handle each
        businessName = returnHeader.find("filer").find("businessname").find("businessnameline1txt")
        if(businessName):
            data_dict["BusinessName"] = businessName.find(text=True)
        else:
            data_dict["BusinessName"] = returnHeader.find("filer").find("businessname").find("businessnameline1").find(text=True)


        #Get preparer firm name. There are multiple formats. Handle each. If there is no preparer firm then it's handled in 'except' clause
        try:
            preparerName = returnHeader.find("preparerfirmgrp").find("preparerfirmname").find("businessnameline1txt")
            if(preparerName):
                data_dict["PreparerFirmName"] = preparerName.find(text=True)
            else:
                data_dict["PreparerFirmName"] = returnHeader.find("preparerfirmgrp").find("preparerfirmname").find("businessnameline1").find(text=True)
        except AttributeError:
            data_dict["PreparerFirmName"] = ""

        #Get Return Time Stamp
        data_dict["ReturnTs"] = returnHeader.find("returnts").find(text=True)

        #Get tax year
        data_dict["TaxYr"] = returnHeader.find("taxyr").find(text=True)

        #Get gross receipts amount
        data_dict["GrossReceiptsAmt"] = returnData.find("irs990").find("grossreceiptsamt").find(text=True)

        master_data_dict_list.append(data_dict)

    #Note: In this short excercise, data is parsed only if it is an IRS990 return. Otherwise its discarded
    except AttributeError:
        print("")
    #For other unexpected errors
    except:
        print('Exception for url ' + mainURLList[urlNumber])
        print('The exception is: ' + str(sys.exc_info()[0]))



# Write to csv file
with open(IRS_990_CSV_FILE, 'w') as csvfile:
    fieldnames = ["EIN", "BusinessName", "PreparerFirmName", "ReturnTs", "TaxYr", "GrossReceiptsAmt"]
    writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=fieldnames)
    writer.writeheader()

    for row in master_data_dict_list:
        writer.writerow(row)



